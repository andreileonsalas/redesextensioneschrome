 /*
 chrome.storage.sync.get("color", ({ color }) => {
  changeColor.style.backgroundColor = color;
 });*/

 // When the button is clicked, inject setPageBackgroundColor into current page
 changeColor.addEventListener("click", async () => {

   showDuck();

});



function showDuck() {
   let resultado = document.getElementById('resultado')
   resultado.innerHTML = '';
   var requestOptions = {
       'Content-Type': 'application/json',
       method: 'GET',
       redirect: 'follow'
   };

   fetch("https://random-d.uk/api/random", requestOptions)
       .then(response => response.json())
       .then(result => {
               let duckImg = document.createElement('img');
               duckImg.src = result.url;
               duckImg.style.height = '200px';
               duckImg.style.width = '200px';
               document.getElementById('resultado').appendChild(duckImg);

           }

       )
       .catch(error => console.log('error', error));

}