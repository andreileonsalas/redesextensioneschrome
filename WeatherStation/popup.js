document.getElementById("get_btn").addEventListener("click", load);
window.onload = load;

/**
 * Open Weather API variables to get humidity and temperature
 * Latitude and longitud are from Cuidad Quesada, Costa Rica.
 */
const LAT = "10.3593";
const LON = "-84.5117";
const EXCLUDE = "hourly,daily";
const APPID = "a86b2d011e4791f02a46cc6bfa4afd3a";

/**
 * 
 */
async function getWeatherData() {
  const URL = `https://api.openweathermap.org/data/2.5/onecall?lat=${LAT}&lon=${LON}&exclude=${EXCLUDE}&appid=${APPID}`;
  const response = await fetch(URL, { method: "GET", redirect: "follow" });
  return await response.json();
}

/**
 * 
 */
async function load() {

  document.getElementById("loader_div").style.display = "block";
  document.getElementById("data_div").style.display = "none";

  const data = await getWeatherData();
  if (data.cod != 400) {
    document.getElementById("humidity").innerHTML = `Humidity: ${data["current"]["humidity"]}`
    document.getElementById("temperature").innerHTML = `Temperature: ${data["current"]["temp"]}`;
  } else {
    document.getElementById("humidity").innerHTML = `Couldn't get data from Open Weather :(`;
    document.getElementById("temperature").innerHTML = ``;
  }
  document.getElementById("loader_div").style.display = "none";
  document.getElementById("data_div").style.display = "block";
}
