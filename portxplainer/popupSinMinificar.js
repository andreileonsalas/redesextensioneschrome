const ports = {
    "0": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Reserved",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "1": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "TCP Port Service Multiplexer (TCPMUX)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "2": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "CompressNET 2 Management Utility 3",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "3": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "CompressNET 2 Compression Process 4",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "4": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "5": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Remote Job Entry",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "7": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Echo Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "8": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "9": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Discard Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "10": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "11": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Active Users (systat service) 5 6",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "12": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "13": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Daytime Protocol (RFC 867)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "14": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "15": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Previously netstat service 5",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "16": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "17": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Quote of the Day",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "18": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Message Send Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "19": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Character Generator Protocol (CHARGEN)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "20": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "FTP—data transfer",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "21": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "FTP—control (command)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "22": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Secure Shell (SSH)—used for secure logins, file transfers (scp, sftp) and port forwarding",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "23": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Telnet protocol, unencrypted text communications",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "24": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Priv-mail : any private mail system. citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "25": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Simple Mail Transfer Protocol (SMTP)—used for e-mail routing between mail servers",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "26": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "27": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NSW User System FE citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "29": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "MSG ICP citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "33": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Display Support Protocol citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "34": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Remote File (RF)—used to transfer files between machines citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "35": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Any private printer server protocol citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "37": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "TIME protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "39": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Resource Location Protocol 7 (RLP)—used for determining the location of higher level services from hosts on a network",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "40": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Unassigned",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "42": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ARPA Host Name Server Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "43": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "WHOIS protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "47": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NI FTP 7",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "49": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "TACACS Login Host protocol citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "50": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Remote Mail Checking Protocol 8",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "51": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "IMP Logical Address Maintenance citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "52": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "XNS (Xerox Network Systems) Time Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "53": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Domain Name System (DNS)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "54": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "XNS (Xerox Network Systems) Clearinghouse",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "55": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ISI Graphics Language (ISI-GL) citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "56": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "XNS (Xerox Network Systems) Authentication",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "57": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Mail Transfer Protocol (RFC 780)",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "58": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "XNS (Xerox Network Systems) Mail",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "67": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Bootstrap Protocol (BOOTP) Server; also used by Dynamic Host Configuration Protocol (DHCP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "68": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Bootstrap Protocol (BOOTP) Client; also used by Dynamic Host Configuration Protocol (DHCP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "69": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Trivial File Transfer Protocol (TFTP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "70": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Gopher protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "71": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "NETRJS protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "72": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "NETRJS protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "73": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "NETRJS protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "74": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "NETRJS protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "79": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Finger protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "80": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Hypertext Transfer Protocol (HTTP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "81": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Torpark—Onion routing",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "82": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Torpark—Control",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "88": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Kerberos—authentication system citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "90": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "dnsix (DoD Network Security for Information Exchange) Securit Attribute Token Map citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "99": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "WIP Message protocol",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "101": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "NIC host name",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "102": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "ISO-TSAP (Transport Service Access Point) Class 0 protocol 10",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "104": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ACR/NEMA Digital Imaging and Communications in Medicine",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "105": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "CCSO Nameserver Protocol (Qi/Ph)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "107": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Remote TELNET Service 11 protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "108": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SNA Gateway Access Server 12",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "109": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Post Office Protocol v2 (POP2)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "110": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Post Office Protocol v3 (POP3)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "111": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ONC RPC (SunRPC)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "113": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "ident—Authentication Service/Identification Protocol, 13 used by IRC servers to identify users",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "115": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Simple File Transfer Protocol (SFTP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "117": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "UUCP Path Service",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "118": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SQL (Structured Query Language) Services",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "119": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Network News Transfer Protocol (NNTP)—retrieval of newsgroup messages",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "123": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Network Time Protocol (NTP)—used for time synchronization",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "135": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "DCE endpoint resolution",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "137": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NetBIOS NetBIOS Name Service",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "138": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NetBIOS NetBIOS Datagram Service",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "139": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NetBIOS NetBIOS Session Service",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "143": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Internet Message Access Protocol (IMAP)—management of email messages",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "152": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Background File Transfer Program (BFTP) 15",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "153": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SGMP, Simple Gateway Monitoring Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "156": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SQL Service",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "158": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "DMSP, Distributed Mail Service Protocol 16",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "161": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Simple Network Management Protocol (SNMP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "162": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Simple Network Management Protocol Trap (SNMPTRAP) 17",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "170": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Print-srv, Network PostScript citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "175": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "VMNET (IBM z/VM, z/OS & z/VSE – Network Job Entry(NJE)) citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "177": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "X Display Manager Control Protocol (XDMCP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "179": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "BGP (Border Gateway Protocol)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "194": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Internet Relay Chat (IRC)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "199": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SMUX, SNMP Unix Multiplexer",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "201": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "AppleTalk Routing Maintenance",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "209": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "The Quick Mail Transfer Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "210": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ANSI Z39.50",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "213": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Internetwork Packet Exchange (IPX)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "218": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Message posting protocol (MPP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "220": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Internet Message Access Protocol (IMAP), version 3",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "256": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "2DEV “2SP” Port citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "259": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ESRO, Efficient Short Remote Operations citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "264": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "BGMP, Border Gateway Multicast Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "280": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "http-mgmt",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "308": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Novastor Online Backup",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "311": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Mac OS X Server Admin (officially AppleShare IP Web administration) citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "318": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "PKIX TSP, Time Stamp Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "319": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Precision time protocol event messages",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "320": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Precision time protocol general messages",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "323": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "IMMP, Internet Message Mapping Protocol citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "350": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "MATIP-Type A, Mapping of Airline Traffic over Internet Protocol citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "351": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "MATIP-Type B, Mapping of Airline Traffic over Internet Protocol citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "366": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ODMR, On-Demand Mail Relay citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "369": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Rpc2portmap citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "370": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "codaauth2—Coda authentication server citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "371": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "ClearCase albd citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "383": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "HP data alarm manager citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "384": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "A Remote Network Server System citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "387": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "AURP, AppleTalk Update-based Routing Protocol 19",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "389": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Lightweight Directory Access Protocol (LDAP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "401": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "UPS Uninterruptible Power Supply citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "402": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Altiris, Altiris Deployment Client citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "411": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Direct Connect Hub",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "412": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Direct Connect Client-to-Client",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "427": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Service Location Protocol (SLP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "443": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "HTTPS (Hypertext Transfer Protocol over SSL/TLS)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "444": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SNPP, Simple Network Paging Protocol (RFC 1568)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "445": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Microsoft-DS Active Directory, Windows shares",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "464": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Kerberos Change/Set password",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "465": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Cisco protocol citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "475": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "tcpnethaspsrv (Aladdin Knowledge Systems Hasp services, TCP/IP version) citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "497": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Dantz Retrospect",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "500": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Qmatic Qwin communication port citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "501": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "STMF, Simple Transportation Management Framework—DOT NTCIP 1101 citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "502": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "asa-appl-proto, Protocol citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "504": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Citadel—multiservice protocol for dedicated clients for the Citadel groupware system",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "510": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "First Class Protocol citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "512": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Rexec, Remote Process Execution",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "513": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "rlogin",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "514": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Shell—used to execute non-interactive commands on a remote system (Remote Shell, rsh, remsh)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "515": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Line Printer Daemon—print service",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "517": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Talk citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "518": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "NTalk citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "520": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "efs, extended file name server citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "524": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NetWare Core Protocol (NCP) is used for a variety things such as access to primary NetWare server resources, Time Synchronization, etc.",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "525": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Timed, Timeserver citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "530": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "RPC citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "531": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "AOL Instant Messenger",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "532": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "netnews citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "533": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "netwall, For Emergency Broadcasts citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "540": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "UUCP (Unix-to-Unix Copy Protocol)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "542": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "commerce (Commerce Applications) citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "543": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "klogin, Kerberos login",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "544": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "kshell, Kerberos Remote shell",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "545": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "OSIsoft PI (VMS), OSISoft PI Server Client Access",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "546": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "DHCPv6 client",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "547": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "DHCPv6 server",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "548": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Apple Filing Protocol (AFP) over TCP",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "550": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "new-rwho, new-who citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "554": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Real Time Streaming Protocol (RTSP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "556": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Remotefs, RFS, rfs_server",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "560": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "rmonitor, Remote Monitor citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "561": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "monitor citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "563": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NNTP protocol over TLS/SSL (NNTPS)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "587": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "e-mail message submission 20 (SMTP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "591": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "FileMaker 6.0 (and later) Web Sharing (HTTP Alternate, also see port 80)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "593": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "HTTP RPC Ep Map, Remote procedure call over Hypertext Transfer Protocol, often used by Distributed Component Object Model services and Microsoft Exchange Server",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "604": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "TUNNEL profile, 21 a protocol for BEEP peers to form an application layer tunnel",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "623": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "ASF Remote Management and Control Protocol (ASF-RMCP) citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "631": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Internet Printing Protocol (IPP)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "635": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "RLZ DBase citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "636": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Lightweight Directory Access Protocol over TLS/SSL (LDAPS)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "639": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "MSDP, Multicast Source Discovery Protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "641": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SupportSoft Nexus Remote Command (control/listening): A proxy gateway connecting remote control traffic citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "646": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "LDP, Label Distribution Protocol, a routing protocol used in MPLS networks",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "647": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "DHCP Failover protocol 22",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "648": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "RRP (Registry Registrar Protocol) 23",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "651": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "IEEE-MMS citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "652": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "DTCP, Dynamic Tunnel Configuration Protocol citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "653": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "SupportSoft Nexus Remote Command (data): A proxy gateway connecting remote control traffic citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "654": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Media Management System (MMS) Media Management Protocol (MMP) 24",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "657": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "IBM RMC (Remote monitoring and Control) protocol, used by System p5 AIX Integrated Virtualization Manager (IVM) 25 and Hardware Management Console to connect managed logical partitions (LPAR) to enable dynamic partition reconfiguration",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "660": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Mac OS X Server administration",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "665": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "sun-dr, Remote Dynamic Reconfiguration citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "666": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Doom, first online first-person shooter",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "674": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "ACAP (Application Configuration Access Protocol)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "691": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "MS Exchange Routing",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "694": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Linux-HA High availability Heartbeat",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "695": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "IEEE-MMS-SSL (IEEE Media Management System over SSL) 26",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "698": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "OLSR (Optimized Link State Routing)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "700": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "EPP (Extensible Provisioning Protocol), a protocol for communication between domain name registries and registrars (RFC 5734)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "701": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "LMP (Link Management Protocol (Internet)), 27 a protocol that runs between a pair of nodes and is used to manage traffic engineering (TE) links",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "702": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "IRIS 28 29 (Internet Registry Information Service) over BEEP (Blocks Extensible Exchange Protocol) 30 (RFC 3983)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "706": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Secure Internet Live Conferencing (SILC)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "711": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Cisco Tag Distribution Protocol 31 32 33 —being replaced by the MPLS Label Distribution Protocol 34",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "712": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Topology Broadcast based on Reverse-Path Forwarding routing protocol (TBRPF) (RFC 3684)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "720": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "SMQP, Simple Message Queue Protocol citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "749": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Kerberos (protocol) administration",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "750": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "kerberos-iv, Kerberos version IV",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "751": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "kerberos_master, Kerberos authentication",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "752": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "passwd_server, Kerberos Password (kpasswd) server",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "753": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Reverse Routing Header (rrh) 35",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "754": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "tell send citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "760": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "krbupdate kreg , Kerberos registration",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "782": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Conserver serial-console management server",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "783": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "SpamAssassin spamd daemon",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "808": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Microsoft Net.TCP Port Sharing Service citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "829": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "CMP (Certificate Management Protocol) citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "843": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Adobe Flash 36",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "847": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "DHCP Failover protocol",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "848": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Group Domain Of Interpretation (GDOI) protocol citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "860": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "iSCSI (RFC 3720)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "873": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "rsync file synchronisation protocol",
          "FIELD5": "Official USA only"
       }
    ,
    "888": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "cddbp, CD DataBase (CDDB) protocol (CDDBP)—unassigned but widespread use",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "901": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Samba Web Administration Tool (SWAT)",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "902": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "ideafarm-door citation needed",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "903": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "VMware Remote Console 37",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "904": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "VMware Server Alternate (if 902 is in use, i.e. SUSE linux)",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "911": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Network Console on Acid (NCA)—local tty redirection over OpenSSH",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "944": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Network File System (protocol) Service",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "953": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "Domain Name System (DNS) RNDC Service",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "973": 
       {
          "Protocol": "",
          "Description": "UDP",
          "Status": "Network File System (protocol) over IPv6 Service",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "981": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "SofaWare Technologies Remote HTTPS management for firewall devices running embedded Check Point FireWall-1 software",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "987": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Microsoft This Secure Hypertext Transfer Protocol (HTTPS) port makes Windows SharePoint Services viewable through Remote Web Workplace citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "989": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "FTPS Protocol (data): FTP over TLS/SSL",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "990": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "FTPS Protocol (control): FTP over TLS/SSL",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "991": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "NAS (Netnews Administration System) 38",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "992": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "TELNET protocol over TLS/SSL",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "993": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Internet Message Access Protocol over SSL (IMAPS)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "995": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Post Office Protocol 3 over TLS/SSL (POP3S)",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    ,
    "999": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "ScimoreDB Database System citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "1002": 
       {
          "Protocol": "TCP",
          "Description": "",
          "Status": "Opsware agent (aka cogbot) citation needed",
          "FIELD5": "Port is not officialy registered by IANA for this application"
       }
    ,
    "1023": 
       {
          "Protocol": "TCP",
          "Description": "UDP",
          "Status": "The Internet Assigned Numbers Authority (IANA) port number reserver",
          "FIELD5": "Port is officialy registered by IANA for this application"
       }
    
 }

/*
chrome.storage.sync.get("color", ({ color }) => {
  changeColor.style.backgroundColor = color;
});*/

// When the button is clicked, inject setPageBackgroundColor into current page
changeColor.addEventListener("click", async () => {
    puertoSeleccionado = document.getElementById('puertoInput').value
    showResults(puertoSeleccionado);
    /*let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      function: setPageBackgroundColor,
    });*/
  });
  
  // The body of this function will be executed as a content script inside the
  // current page
  /*
  function setPageBackgroundColor() {
    chrome.storage.sync.get("color", ({ color }) => {
      document.body.style.backgroundColor = color;
    });
    
  }*/

  function showResults(port){
    let resultado = document.getElementById('resultado')
    resultado.innerHTML = ''
    if (ports[port] !== undefined) {
        let portDiv = document.createElement("div");
        let newContent = document.createTextNode("Port: " + port);
        portDiv.appendChild(newContent); //añade texto al div creado.
        resultado.appendChild(portDiv);
    
        let protocolo = document.createElement("div");
        let newContent2 = document.createTextNode("Protocol: " + ports[port].Protocol);
        protocolo.appendChild(newContent2); //añade texto al div creado.
        resultado.appendChild(protocolo);
    
        let description = document.createElement("div");
        let newContent3 = document.createTextNode("Aditional protocol: " + ports[port].Description);
        description.appendChild(newContent3); //añade texto al div creado.
        resultado.appendChild(description);
    
        let status = document.createElement("div");
        let newContent4 = document.createTextNode("Description: " + ports[port].Status);
        status.appendChild(newContent4); //añade texto al div creado.
        resultado.appendChild(status);

        let oficialIANA = document.createElement("div");
        let newContent5 = document.createTextNode("IANA status: " + ports[port].FIELD5);
        oficialIANA.appendChild(newContent5); //añade texto al div creado.
        resultado.appendChild(oficialIANA);
    }
    else    {
        resultado.innerHTML = 'There is no oficial information about that port or is not in the first 1023 ports'
    }

  }