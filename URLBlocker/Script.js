document.getElementById("botonGuarda").addEventListener("click", guardaArchivo);
document.getElementById("botonCarga").addEventListener("click", cargarDatos);
document.getElementById("printer").addEventListener("click", print);

function guardaArchivo(){
    var datosGuardar = document.getElementById("urls").value;  
    var temp = document.createElement("a");
    temp.href = window.URL.createObjectURL(new Blob([datosGuardar], {type: "text/plain"}));
    temp.download = "archivoURLs.txt";
    temp.click();
}

function cargarDatos() {
    var fileLoad = document.querySelector('input').files[0];
    var reader = new FileReader();
    reader.addEventListener("loadend", function() {
        document.getElementById('fileChoosen').innerText = reader.result;
        localStorage.setItem('blockPages',reader.result);   
        alert("Se ha cargado: " + reader.result)  
    });
    reader.readAsText(fileLoad)
}

function print(){
    alert(localStorage.getItem('blockPages'))
}